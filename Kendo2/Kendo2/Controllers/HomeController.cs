﻿using Kendo2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Kendo2.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Read()
        {
            KendoEntities db = new KendoEntities();
            var result = db.Products.AsEnumerable();
            return Json(result,JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult AddProduct(Products newProduct)
        {
            if(ModelState.IsValid)
            {
                var dbProduct = new KendoEntities();
                
                    dbProduct.Products.Add(newProduct);
                    dbProduct.SaveChanges();

                return Json(newProduct);
            }
            return Json(null);
        }

        [HttpPost]
        public ActionResult Destroy(Products product)
        {
            var dbProduct = new KendoEntities();

            var myProduct = dbProduct.Products.Find(product.ProductId);
            dbProduct.Products.Remove(myProduct);
            dbProduct.SaveChanges();

            return Json(product);
        }
    }
}